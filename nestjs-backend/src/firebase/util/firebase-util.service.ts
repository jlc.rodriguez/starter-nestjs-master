import * as admin from 'firebase-admin';
import {Injectable} from '@nestjs/common';
import {FirebaseUtil} from '@manticore-labs/firebase-nest'

@Injectable()
export class FirebaseUtilService extends FirebaseUtil {
    constructor() {
        super(admin.firestore);
    }
}
