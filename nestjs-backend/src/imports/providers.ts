import {AppService} from '../app.service';
import {SeguridadGuard} from '../guards/seguridad/seguridad.guard';

export const PROVIDERS = [
    AppService,
    SeguridadGuard
];
