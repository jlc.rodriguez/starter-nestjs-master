import {CONFIG} from '../../environment/config';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ENTITIES_SEGURIDAD} from './entities-seguridad';

export const CONEXION_BASE_SEGURIDAD = TypeOrmModule.forRoot({
    type: CONFIG.bdd_relacional.DB_TYPE as 'mysql',
    name: CONFIG.seguridad.DB_NOMBRE_CONEXION,
    host: CONFIG.bdd_relacional.DB_HOST,
    port: CONFIG.bdd_relacional.DB_PORT,
    username: CONFIG.seguridad.DB_USERNAME,
    password: CONFIG.seguridad.DB_PASSWORD,
    database: CONFIG.seguridad.DB_DATABASE,
    // SOLO PARA ORACLE
    // connectString:
    //     '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = ' +
    //     CONFIG.bdd_relacional.DB_HOST +
    //     ')(PORT = ' +
    //     CONFIG.bdd_relacional.DB_PORT +
    //     ')) (CONNECT_DATA = (SID = ' +
    //     CONFIG.bdd_relacional.DB_ID +
    //     ')))',
    entities: [
        ...ENTITIES_SEGURIDAD,
    ],
    synchronize: CONFIG.bdd_relacional.DB_SYNCRHONIZE,
    ssl: CONFIG.bdd_relacional.DB_SSL,
    keepConnectionAlive: CONFIG.bdd_relacional.DB_KEEP_CONNECTION_ALIVE,
    retryDelay: CONFIG.bdd_relacional.DB_RETRY_DELAY,
    dropSchema: CONFIG.bdd_relacional.DB_DROPSCHEMA,
    retryAttempts: CONFIG.bdd_relacional.DB_RETRY_ATTEMPTS,
    connectTimeout: CONFIG.bdd_relacional.DB_CONNECTION_TIMEOUT,
    charset: CONFIG.bdd_relacional.DB_CHARSET,
    timezone: CONFIG.bdd_relacional.DB_TIMEZONE,
});

